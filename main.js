var email = document.getElementById("mail");
var phone = document.getElementById("phone");

email.addEventListener("input", function (event) {
    if (email.validity.typeMismatch) {
        email.setCustomValidity("Bad email");
    } else {
        email.setCustomValidity("");
    }
});
phone.addEventListener("input", function (event) {
    if (phone.validity.typeMismatch) {
        phone.setCustomValidity("Bad phone");
    } else {
        phone.setCustomValidity("");
    }
});